<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    
    public function index(Request $request, Session $session)
    {
        return $this->render('home.html.twig', [
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/HomeController.php',
            'test' => $request->query->get('foo'),
            'session' => $session->has('cle_inexistante')
        ]);
    }
}
